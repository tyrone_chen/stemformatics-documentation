.. Stemformatics Documentation master file, created by
   sphinx-quickstart on Thu Nov  8 12:59:59 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Stemformatics Documentation's documentation!
=======================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
